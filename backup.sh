#!/bin/sh
FILENAME=$(date +"%m-%d-%y_%T").tgz
mongodump --host $MONGODB_HOST && \
tar -czvf ./$FILENAME ./dump && \
aws s3 cp ./$FILENAME s3://$S3_BUCKET_NAME/ && \
rm -f $FILENAME