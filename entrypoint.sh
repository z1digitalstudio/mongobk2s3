#!/bin/sh
cp "/usr/share/zoneinfo/${TIMEZONE}" /etc/localtime
echo "${CRON_SCHEDULE} /app/backup.sh" >> /etc/crontabs/root
/usr/sbin/crond -f -l 8