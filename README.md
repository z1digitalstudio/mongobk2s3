# Mongo Backup to S3

## How to use this image

```yaml
mongobk-worker:
  image: registry.gitlab.com/commite/mongobk2s3:latest
  depends_on:
    - mongodb
  environment:
    - MONGODB_HOST=my-db-host:27017
    - S3_BUCKET_NAME=my-bucket
    - TIMEZONE=America/New_York
    - CRON_SCHEDULE=20 8 * * *
```

Must also provide needed AWS credentials in any way, [using env vars](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) or adding `credentials` and `config` files to `/root/.aws/` container folder.
