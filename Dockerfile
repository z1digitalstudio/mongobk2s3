FROM alpine:3.9
RUN apk add --no-cache mongodb-tools py2-pip tzdata && \
  pip install pymongo awscli
WORKDIR /app
ADD entrypoint.sh .
ADD backup.sh .
RUN chmod 755 backup.sh
CMD ["sh", "entrypoint.sh"]
